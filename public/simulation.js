var Simulator = function () {

    var _settings;
    var _lastStep;
    var _stepNumber;

    var G = 6.667408e-11;
    var sun = { m: 1.9884e30, x: 0, y: 0 };
    var planets = [
        { name: "Merkury", color: "#777744", size: 2.440e6, m: 3.3011e23, realMeanV: 47.36e3, x: 57.9e9, y: 0, v: 47851, vx: 0 },
        { name: "Venus", color: "#ff0000", size: 6.051e6, m: 4.867e24, realMeanV: 35.02e3, x: 108.2e9, y: 0, v: 35003, vx: 0 },
        { name: "Earth", color: "#5555ff", size: 6.371e6, m: 5.97219e24, realMeanV: 29.78e3, x: 149.6e9, y: 0, v: 29769, vx: 0 },
        { name: "Mars", color: "#ff6600", size: 3.390e6, m: 6.39e23, realMeanV: 24.07e3, x: 227.9e9, y: 0, v: 24118, vx: 0 },
        { name: "Jupiter", color: "#ffcc99", size: 69.911e6, m: 1.89819e27, realMeanV: 13.06e3, x: 778.5e9, y: 0, v: 13049.7, vx: 0 },
        { name: "Moon", color: "#666666", size: 1.737e6, m: 7.347673e22, realMeanV: 1.023e3, x: 149.6e9, y: 384.4e6, v: 29769, vx: 1017.8, isMoon: true, parentIndex: 2, distanceScale: 50 },
        { name: "Io", color: "#ff9900", size: 1.822e6, m: 8.93e22, realMeanV: 17.34e3, x: 778.5e9, y: 421.8e6, v: 13049.7, vx: 17321.9, isMoon: true, isJupiterMoon: true, parentIndex: 4, distanceScale: 200 },
        { name: "Europa", color: "#999999", size: 1.561e6, m: 4.8e22, realMeanV: 13.74e3, x: 778.5e9, y: 671.1e6, v: 13049.7, vx: 13732.7, isMoon: true, isJupiterMoon: true, parentIndex: 4, distanceScale: 200 },
        { name: "Ganymede", color: "#99cc99", size: 2.631e6, m: 14.8e22, realMeanV: 10.88e3, x: 778.5e9, y: 1070.4e6, v: 13049.7, vx: 10873.6, isMoon: true, isJupiterMoon: true, parentIndex: 4, distanceScale: 200 },
        { name: "Callisto", color: "#777744", size: 2.411e6, m: 10.8e22, realMeanV: 8.204e3, x: 778.5e9, y: 1882.7e6, v: 13049.7, vx: 8198.9, isMoon: true, isJupiterMoon: true, parentIndex: 4, distanceScale: 200 }
    ];

    return {

        GetPlanets: function () {
            return planets;
        },

        GetDay: function () {
            return _stepNumber * _settings.dt / 24 / 60 / 60;
        },

        Start: function (settings) {
            _settings = settings;
            _stepNumber = 0;
            _lastStep = planets
                .filter(function (planet) {
                    return _settings.showMoons || planet.isJupiterMoon === undefined;
                })
                .map(function (planet) {
                    return { x: planet.x, y: planet.y, vy: planet.v, vx: planet.vx, m: planet.m, meanV: planet.v, meanR: planet.x, planet: planet, vxFromPlanet: planet.vx, vyFromPlanet: 0 }
                });
            return _lastStep;
        },

        PerformStep: function () {
            if (_settings.stopAfter > 0 && (_settings.stopAfter * 24 * 60 * 60) <= (_stepNumber * _settings.dt)) {
                return _lastStep;
            }

            _stepNumber++;

            var newStep = planets
                .filter(function (planet) {
                    return _settings.showMoons || planet.isJupiterMoon === undefined;
                }).map(function (planet, i) {
                    var objects = _settings.ignorePlanets ? [] : excludeIndex(_lastStep, i);
                    objects.push(sun);
                    var a = getAcceleration(_lastStep[i], objects);
                    var vx = _lastStep[i].vx - _settings.dt * a.ax;
                    var vy = _lastStep[i].vy - _settings.dt * a.ay;
                    var vValue = 0;
                    // FOR MOON
                    var vxFromPlanet = 0;
                    var vyFromPlanet = 0;
                    //
                    if (planet.isMoon === undefined) {
                        vValue = Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
                    }
                    else { // FOR MOON
                        var aFromPlanet = getAcceleration(_lastStep[i], [_lastStep[_lastStep[i].planet.parentIndex]]);
                        vxFromPlanet = _lastStep[i].vxFromPlanet - _settings.dt * aFromPlanet.ax;
                        vyFromPlanet = _lastStep[i].vyFromPlanet - _settings.dt * aFromPlanet.ay;
                        vValue = Math.sqrt(Math.pow(vxFromPlanet, 2) + Math.pow(vyFromPlanet, 2));
                    }
                    x = _lastStep[i].x + _settings.dt * vx;
                    y = _lastStep[i].y + _settings.dt * vy;
                    r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
                    return {
                        x: x,
                        y: y,
                        vx: vx,
                        vy: vy,
                        m: _lastStep[i].m,
                        meanV: ((_stepNumber - 1) * _lastStep[i].meanV + vValue) / _stepNumber,
                        meanR: ((_stepNumber - 1) * _lastStep[i].meanR + r) / _stepNumber,
                        planet: planet,
                        // FOR MOON
                        vxFromPlanet: vxFromPlanet,
                        vyFromPlanet: vyFromPlanet
                    };
                });

            _lastStep = newStep;
            return newStep;
        }
    };

    // Extra functions    
    function getAcceleration(object, others) {
        var accelerations = others.map(function (o) {
            var direction = Math.atan2(object.y - o.y, object.x - o.x);
            var magnitude = Math.sqrt(Math.pow(o.x - object.x, 2) + Math.pow(o.y - object.y, 2));
            a = G * o.m / Math.pow(magnitude, 2);
            return {
                ax: Math.cos(direction) * a,
                ay: Math.sin(direction) * a
            };
        });

        return accelerations.reduce(function (acc, val) { return { ax: acc.ax + val.ax, ay: acc.ay + val.ay } });
    }

    function excludeIndex(arr, index) {
        return arr.filter(function (value, arrIndex) {
            return index !== arrIndex;
        });
    }
}();