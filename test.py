#GlowScript 2.7 VPython
from vpython import *
scene.caption = """In GlowScript programs:
Right button drag or Ctrl-drag to rotate "camera" to view scene.
To zoom, drag with middle button or Alt/Option depressed, or use scroll wheel.
  On a two-button mouse, middle is left + right.
Shift-drag to pan left/right and up/down.
Touch screen: pinch/extend to zoom, swipe or two-finger rotate."""
scene.forward = vector(0,-.3,-1)

G = 6.667408e-11 # Newton gravitational constant
fac = 10
sun = sphere(pos=vector(0,0,0), radius=2e10, color=color.red)
sun.mass = 1.9884e30


earth = sphere(pos=vector(152100000000,0,0), radius=6.9e9, color=color.blue)
earth.mass = 5.97219e24
earth.p = vector(0,0,29784.1)* earth.mass

merkury = sphere(pos=vector(57910000000,0,0), radius=3e9, color=color.green)
merkury.mass = 3.3011e23
merkury.p = vector(0,0,50000)* merkury.mass

venus = sphere(pos=vector(108200000000,0,0), radius=6e9, color=color.yellow)
venus.mass = 4.867e24
venus.p = vector(0,0,35000)* venus.mass


dt = 60 * 60
while True:
    rate(100)
    r1 = earth.pos - sun.pos
    F1 = G * sun.mass * earth.mass * r1.hat / mag2(r1)
    r2 = merkury.pos - sun.pos
    F2 = G * sun.mass * merkury.mass * r2.hat / mag2(r2)
    r3 = venus.pos - sun.pos
    F3 = G * sun.mass * venus.mass * r3.hat / mag2(r3)
    earth.p = earth.p - F1*dt
    earth.pos = earth.pos + (earth.p/earth.mass) * dt
    merkury.p = merkury.p - F2 * dt
    merkury.pos = merkury.pos + (merkury.p / merkury.mass) * dt
    venus.p = venus.p - F3 * dt
    venus.pos = venus.pos + (venus.p / venus.mass) * dt

